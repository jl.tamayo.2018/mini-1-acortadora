#!/usr/bin/python

from webapp import Webapp
import random
import string
from urllib import parse
import shelve

PORT = 1234

PREFIX1 = "http://"
PREFIX2 = "https://"

url_database = shelve.open('urls')

PAGE = """
<html>
    <body>
        {content}
        <hr>
        {form}
    </body>
</html>
"""

PAGE_NOT_FOUND = """
<html>
    <body>
        Resource not found: {resource}
        <hr>
        {form}
    </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<html>
    <body>
        Method not found: {method}
    </body>
</html>
"""

FORM = """
<form action='/' method="POST">
    <div>
        <label>URL a acortar: </label>
        <input type="text" name="url" required>
    </div>
    <div>
        <input type="submit" value="SEND">
    </div>
</form>
"""

PRINCIPAL_PAGE = """
<html>
    <body>
        <h1>
            URL SHORTENER WEBSITE
        </h1>
        <div>
           <p> 
            <font size="5" align="center"> URL LIST: <br><br>{url_list} </font>
           </p>
        </div>
        <div>
            {form}
        </div>
    </body>
</html>
"""

PAGE_POST = """
<html>
    <body>
        <h1>
            URL SHORTENER WEBSITE
        </h1>
        <div>
           <p> 
            <font size="5" align="center"> URL LIST:<br><br> {url_list} </font>
           </p>
        </div>
        <div>
            {form}
        </div>
    </body>
</html>
"""

PAGE_UNPROCESSABLE = """
<html>
    <body>>
        <h1>
            Unprocessable, something wrong: {body}
        </h1>
    </body>
</html>
"""


class Randomshort(Webapp):

    def parse(self, request):
        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            # no hay cuerpo en la petición
            data['body'] = None
        else:
            # hay cuerpo en la petición
            data['body'] = request[body_start + 4:]

        parts = request.split(' ', 2)
        data['method'] = parts[0]
        data['resource'] = parts[1]
        return data

    def process(self, data):
        if data['method'] == 'GET':
            http_code, html_page = self.get(data['resource'])
        elif data['method'] == 'POST':
            http_code, html_page = self.post(data['resource'], data['body'])
        else:
            http_code = "405 Method not allowed"
            html_page = PAGE_NOT_ALLOWED.format(method='POST')

        return http_code, html_page

    def get(self, resource):
        if resource == '/':
            http_code = "200 OK"
            html_page = PRINCIPAL_PAGE.format(url_list=self.print_dictionary(), form=FORM)
        elif resource in url_database:
            url = url_database[resource]
            http_code = "301 Moved Permanently\r\nLocation:" + url
            html_page = ""
        else:
            http_code = "404 NOT FOUND"
            html_page = PAGE_NOT_FOUND.format(resource=resource, form=FORM)
        return http_code, html_page

    def post(self, resource, body):
        fields = parse.parse_qs(body)
        print(fields)
        if resource == '/':
            if 'url' in fields:
                url = self.complete_url(fields['url'][0])
                if url in url_database.values():
                    shorten_url = list(url_database.keys())
                    http_code = "200 OK"
                    html_page = PAGE_POST.format(url_list=self.print_dictionary(), form=FORM)
                else:
                    random_key = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
                    shorten_url = '/' + random_key
                    url_database[shorten_url] = url
                    print(shorten_url)
                    http_code = "200 OK"
                    html_page = PAGE_POST.format(url_list=self.print_dictionary(), form=FORM)
            else:
                html_page = PAGE_UNPROCESSABLE.format(body=body)
                http_code = "422 Unprocessable Entity"
        else:
            http_code = "422 Unprocessable Entity"
            html_page = PAGE_UNPROCESSABLE.format(body=body)
        return http_code, html_page

    def complete_url(self, url):
        if url.startswith((PREFIX1, PREFIX2)):
            return url
        else:
            url = PREFIX2 + url
            return url

    def print_dictionary(self):
        dictionary = ""
        for element in url_database:
            dictionary = dictionary + "Long Url: " + url_database[
                element] + "<br>" + " Short Url: localhost:1234" + element + "<br>"
        return dictionary


if __name__ == "__main__":
    testContentApp = Randomshort("localhost", PORT)
