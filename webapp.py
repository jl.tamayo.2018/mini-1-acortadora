#!/usr/bin/python

import socket


class Webapp:

    def __init__(self, hostname, port):
        """"Iniciar nuestra aplicacion web"""

        # configuración socket
        my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.bind((hostname, port))
        my_socket.listen(5)

        try:
            while True:
                print("Esperando alguna conexion...")
                connection_socket, addr = my_socket.accept()
                print("Conexion recibida de: " + str(addr))
                recibido = connection_socket.recv(2048)

                # manejar/parsear peticion, los recursos
                parse_request = self.parse(recibido.decode('utf-8'))

                # proceso la peticion y creo la respuesta
                return_code, html_respuesta = self.process(parse_request)

                # enviar respuesta
                respuesta = "HTTP/1.1 " + return_code + "\r\n\r\n" \
                            + html_respuesta + "\r\n"
                connection_socket.send(respuesta.encode('utf-8'))
                connection_socket.close()

        except KeyboardInterrupt:
            print("Cerrando servidor", end='')
            my_socket.close()

    def parse(self, request):
        """"Parseo la petición extrayendo la info"""

        parsed_request = request.split()[1].split('/')
        if parsed_request[1] == "":
            print("parse: No hay nada que parsear ahora mismo")

        return parsed_request

    def process(self, parsed_request):
        """"Procesa los datos de la petición

        Devuelve el código HTTP de la respuesta y una página HTML
        """

        return "200 OK", "<html><body>Hola mundo!</body></html>"


if __name__ == "__main__":
    testWebApp = Webapp("localhost", 1234)
